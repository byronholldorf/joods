package com.jood.exception;

public class DBException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public DBException(String msg) {
		super(msg);
	}
	
	public DBException(Throwable t) {
		super(t);
	}
	
	public DBException(String msg, Exception e) {
		super(msg, e);
	}
	
}
