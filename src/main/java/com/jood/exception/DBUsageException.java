package com.jood.exception;

public class DBUsageException extends DBException {

	private static final long serialVersionUID = 1L;

	public DBUsageException(String msg) {
		super(msg);
	}
	
	public DBUsageException(String msg, Exception t) {
		super(msg, t);
	}
}
