package com.jood.exception;

public class DBSerializeException extends DBException {

	private static final long serialVersionUID = 1L;

	public DBSerializeException(String msg) {
		super(msg);
	}
	
	public DBSerializeException(String msg, Exception e) {
		super(msg, e);
	}
	
	public DBSerializeException(Throwable t) {
		super(t);
	}

}
