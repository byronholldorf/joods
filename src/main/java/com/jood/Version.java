package com.jood;

import java.util.ArrayList;
import java.util.List;

import com.jood.data.DBBase;
import com.jood.exception.DBUsageException;

public class Version {

	private final Repository repo;
	private final Long number;
	
	final List<DBBase> changes = new ArrayList<DBBase>();
	final List<DBBase> unchanged = new ArrayList<DBBase>();
	
	Version(Repository repo, long number) {
		this.repo = repo;
		this.number = number;
	}
	
	public <T> T get(Class<T> class1) {
		return getUserRoot(class1);
	}
	
	private <T> T getUserRoot(Class<T> class1) {
		T result = null;
		try {
			result = class1.newInstance();
		} catch (InstantiationException e) {
			throw new DBUsageException("Cannot get root object", e);
		} catch (IllegalAccessException e) {
			throw new DBUsageException("Illegal access for root object", e);
		}
		
		DBBase.initChildren(result, this, null, "ROOT");
		
		return result;
	}

	public long getVersionNumber() {
		return number;
	}

	public CommitResult commitSimple(Conflict use) {
		return repo.commitSimple(this, use);
	}

	public Repository getRepo() {
		return repo;
	}
	
	public void markChanged(DBBase item) {
		changes.add(item);
	}
	
	public void assertUnchanged(DBBase item) {
		unchanged.add(item);
	}
	
}
