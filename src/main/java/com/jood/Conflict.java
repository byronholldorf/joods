package com.jood;

public enum Conflict {
	USE_MINE,
	USE_THEIRS;
}
