package com.jood;

import java.util.concurrent.ThreadPoolExecutor;

import com.jood.command.AssertionCommand;
import com.jood.command.ChangeCommand;
import com.jood.command.ConflictOperation;
import com.jood.command.DatabaseWriteOperation;
import com.jood.data.DBBase;
import com.jood.data.DBId;
import com.jood.persist.DBSerializable;
import com.jood.persist.Publisher;
import com.jood.persist.Reporter;
import com.jood.persist.Serializer;

public class Repository {

	private Publisher publisher;
	private Reporter reporter;
	private Serializer serializer;
	
	private ThreadPoolExecutor pool;

	public Repository(DBSettings settings) {
		publisher = settings.getPublisher();
		reporter = settings.getReporter();
		serializer = settings.getSerializer();
		pool = settings.getWriteOperationThreadpool();
	}
	
	public Version getRecentVersion() {
		return getVersion(reporter.getNewestVersion());
	}

	public Version getVersion(long id) {

		Version version = new Version(this, id);
		return version;
	}

	public void commit(final CommitOperation commitOperation) {
		
		Runnable run = new Runnable() {
			@Override
			public void run() {
				Version version = getRecentVersion();
				commitOperation.commit(version);
				DatabaseWriteOperation op = buildOperation(version,ConflictOperation.RESPONSE);
				publisher.commit(op);
			}
		};
		
		pool.execute(run);
		
		
		//TODO wait for response
	}

	public CommitResult commitSimple(Version version, Conflict use) {

		DatabaseWriteOperation op = buildOperation(version, use == Conflict.USE_MINE?ConflictOperation.NO_CHECK:ConflictOperation.ABORT);
		publisher.commit(op);
		
		return new CommitResult();
	}

	private DatabaseWriteOperation buildOperation(Version version, ConflictOperation conflict) {
		DatabaseWriteOperation op = new DatabaseWriteOperation(conflict);
		
		for(DBBase item:version.unchanged) {
			op.operations.add(new AssertionCommand(item.getId(), item.getVersion().getVersionNumber()));
		}
		
		for(DBBase item:version.changes) {
			op.operations.add(new ChangeCommand(item.getId(), item.getVersion().getVersionNumber(), serializer.serialize(item)));
		}
		
		return op;
	}
	
	public void getObject(DBSerializable obj, DBId dbId, Version version) {
		byte[] data = reporter.getObject(dbId, version.getVersionNumber());
		if(data != null) {
			serializer.deserialize(obj, data);
		}
	}
}
