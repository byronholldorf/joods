package com.jood.persist;

public interface Serializer {

	void deserialize(DBSerializable item, byte[] object);

	byte[] serialize(DBSerializable item);

}
