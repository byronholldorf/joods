package com.jood.persist;

import com.jood.command.DatabaseWriteOperation;

public interface Publisher {

	void commit(DatabaseWriteOperation op);

}
