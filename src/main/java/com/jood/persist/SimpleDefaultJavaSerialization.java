package com.jood.persist;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

import com.jood.exception.DBSerializeException;

public class SimpleDefaultJavaSerialization implements Serializer {
	
	@Override
	public byte[] serialize(DBSerializable obj) {
		try {
			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			
			
			new ObjectOutputStream(baos).writeObject(obj.saveData());
			return baos.toByteArray();
		} catch (Exception e) {
			throw new DBSerializeException("Could not deserialize Object", e);
		}
	}

	@Override
	public void deserialize(DBSerializable item, byte[] data) {
		try {
			ObjectInputStream ois = new ObjectInputStream(new ByteArrayInputStream(data));
			item.loadData(ois.readObject());
		} catch (Exception e) {
			throw new DBSerializeException("Could not serialize Object", e);
		}
	}
}
