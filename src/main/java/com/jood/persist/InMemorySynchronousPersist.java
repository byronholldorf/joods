package com.jood.persist;

import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;

import com.jood.command.ChangeCommand;
import com.jood.command.Command;
import com.jood.command.ConflictOperation;
import com.jood.command.DatabaseWriteOperation;
import com.jood.data.DBId;

public class InMemorySynchronousPersist implements Publisher, Reporter {
	
	private long version;
	
	private final Map<DBId, TreeMap<Long, byte[]>> data = new HashMap<DBId, TreeMap<Long,byte[]>>();
	
	@Override
	public synchronized void commit(DatabaseWriteOperation op) {
		if(op.onConflict == ConflictOperation.ABORT) {
			for(Command c : op.operations) {
				TreeMap<Long, byte[]> tree = data.get(c.getDbid());
				if(tree != null) {
					if(tree.higherEntry(c.getDbVersion()) != null) {
						return; // abort
					}
				}
			}
		}
		
		for(Command c : op.operations) {
			if(c instanceof ChangeCommand) {
				byte[] bytes = ((ChangeCommand)c).getData();
				TreeMap<Long, byte[]> tree = data.get(c.getDbid());
				if(tree == null) {
					tree = new TreeMap<Long, byte[]>();
					data.put(c.getDbid(), tree);
				}
				tree.put(version + 1, bytes);
			}
		}
		
		version++;
	}
	
	@Override
	public synchronized long getNewestVersion() {
		return version;
	}
	
	@Override
	public byte[] getObject(DBId id, long version) {
		TreeMap<Long, byte[]> tree = data.get(id);
		if(tree != null) {
			return tree.floorEntry(version).getValue();
		}
		return null;
	}
	
	
}
