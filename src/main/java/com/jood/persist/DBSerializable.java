package com.jood.persist;

public interface DBSerializable {

	void loadData(Object o);
	Object saveData();
}
