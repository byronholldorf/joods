package com.jood.persist;

import com.jood.data.DBId;

public interface Reporter {

	long getNewestVersion();

	byte[] getObject(DBId id, long version);

}
