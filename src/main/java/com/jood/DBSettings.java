package com.jood;

import java.util.concurrent.ThreadPoolExecutor;

import com.jood.persist.Publisher;
import com.jood.persist.Reporter;
import com.jood.persist.Serializer;

public interface DBSettings {

	Publisher getPublisher();

	Reporter getReporter();

	Serializer getSerializer();

	ThreadPoolExecutor getWriteOperationThreadpool();

}
