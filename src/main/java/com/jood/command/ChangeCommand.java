package com.jood.command;

import com.jood.data.DBId;

public class ChangeCommand extends Command {

	private byte[] serialized;
	
	public ChangeCommand(DBId id, long version, byte[] data) {
		super(id, version);
		this.serialized = data;
	}

	public byte[] getData() {
		return serialized;
	}
}
