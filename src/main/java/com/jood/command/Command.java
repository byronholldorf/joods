package com.jood.command;

import com.jood.data.DBId;

public class Command {
	private DBId dbid;
	private long dbVersion;
	
	public Command(DBId dbid, long dbVersion) {
		this.dbid = dbid;
		this.dbVersion = dbVersion;
	}
	
	public DBId getDbid() {
		return dbid;
	}
	
	public long getDbVersion() {
		return dbVersion;
	}
}
