package com.jood.command;

import java.util.ArrayList;
import java.util.List;

public class DatabaseWriteOperation {

	public final List<Command> operations = new ArrayList<Command>();
	public final ConflictOperation onConflict;
	
	public DatabaseWriteOperation(ConflictOperation onConflict) {
		this.onConflict = onConflict;
	}
}
