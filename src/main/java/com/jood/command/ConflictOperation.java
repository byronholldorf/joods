package com.jood.command;

public enum ConflictOperation {
	NO_CHECK,
	ABORT,
	RESPONSE;
}
