package com.jood;

public interface CommitOperation {

	void commit(Version version);

}
