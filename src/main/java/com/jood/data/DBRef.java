package com.jood.data;

import com.jood.exception.DBException;
import com.jood.exception.DBUsageException;

public class DBRef<T> extends DBBase {

	private T obj;
	private Class<T> cls;
	
	public DBRef(Class<T> cls) {
		this.cls = cls;
	}
	
	public T get() {
		if(obj != null) {
			return obj;
		}
		
		try {
			obj = cls.newInstance();
		} catch (InstantiationException e) {
			throw new DBUsageException("Cannot create DBRef Object", e);
		} catch (IllegalAccessException e) {
			throw new DBUsageException("Illegal access for DBRef Object", e);
		}
		DBBase.initChildren(obj, getVersion(), this, getFieldName());
		return obj;
	}

	@Override
	public void loadData(Object o) {
		throw new DBException("Internal Error");
	}

	@Override
	public Object saveData() {
		throw new DBException("Internal Error");
	}

}
