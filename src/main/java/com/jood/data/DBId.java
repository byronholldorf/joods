package com.jood.data;

public class DBId {

	private final String path;
	
	public DBId(String string) {
		path = string;
	}
	
	public DBId(DBId id, String ownerFieldName) {
		path = id.getPath() + "/" + getPath();
	}

	public String getPath() {
		return path;
	}
	
	@Override
	public boolean equals(Object obj) {
		if(obj instanceof DBId) {
			return ((DBId)obj).getPath().equals(path);
		}
		return false;
	}
	
	@Override
	public int hashCode() {
		return path.hashCode();
	}

}
