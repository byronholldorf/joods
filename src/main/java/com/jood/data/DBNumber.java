package com.jood.data;

import com.jood.Version;

public class DBNumber<T extends Number> extends DBBase {

	private Number number;
	
	@Override
	public void init(Version version, DBBase owner, String name) {
		super.init(version, owner, name);
		version.getRepo().getObject(this, getId(), version);
	}
	
	public void set(T val) {
		number = val;
		this.getVersion().markChanged(this);
		
	}

	public T get() {
		return (T)number;
	}

	@Override
	public void loadData(Object o) {
		number = (Number)o;
	}
	
	@Override
	public Object saveData() {
		return number;
	}
}
