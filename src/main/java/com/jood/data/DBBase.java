package com.jood.data;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;

import com.jood.Version;
import com.jood.exception.DBUsageException;
import com.jood.persist.DBSerializable;

public abstract class DBBase implements DBSerializable {
	private transient DBBase owner;
	private transient String ownerFieldName;
	private transient Version version;
	
	public DBId getId() {
		
		if(owner != null) {
			return new DBId(owner.getId(), ownerFieldName);
		} 
		
		return new DBId("ROOT");
	}
	
	public Version getVersion() {
		return version;
	}
	
	String getFieldName() {
		return ownerFieldName;
	}
	
	
	public static void initChildren(Object obj, Version version, DBBase owner, String name) {
		
		for(Field field:obj.getClass().getDeclaredFields()) {
			if(DBBase.class.isAssignableFrom(field.getType())) {
				
				if(!Modifier.isFinal(field.getModifiers())) {
					throw new DBUsageException(String.format("%s.%s Should be final", field.getClass(), field.getName()));
				}
				
				field.setAccessible(true);
				try {
					DBBase object = (DBBase)field.get(obj);
					object.init(version, owner, field.getName());
				} catch (IllegalAccessException ex) {
					throw new DBUsageException("Illegal access", ex);
				}
				
				
			}
		}
	}
	
	public void init(Version version, DBBase owner, String name) {
		
		this.version = version;
		this.owner = owner;
		this.ownerFieldName = name;
		
		for(Field field:this.getClass().getDeclaredFields()) {
			if(DBBase.class.isAssignableFrom(field.getType())) {
				
				if(!Modifier.isFinal(field.getModifiers())) {
					throw new DBUsageException(String.format("%s.%s Should be final", field.getClass(), field.getName()));
				}
				
				field.setAccessible(true);
				try {
					DBBase object = (DBBase)field.get(this);
					object.init(version, this, field.getName());
				} catch (IllegalAccessException ex) {
					throw new DBUsageException("Illegal access", ex);
				}
				
				
			}
		}
	}

	//TODO dislike name
	public void assertUnchanged() {
		this.getVersion().assertUnchanged(this);
	}
}
