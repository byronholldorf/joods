package com.jood.data;

import com.jood.Version;

public class DBString extends DBBase {

	private String value;
	
	@Override
	public void init(Version version, DBBase owner, String name) {
		super.init(version, owner, name);
		version.getRepo().getObject(this, getId(), version);
	}
	
	public void set(String string) {
		this.value = string;
		this.getVersion().markChanged(this);
	}

	public String get() {
		return value;
	}

	@Override
	public void loadData(Object o) {
		value = (String)o;
	}
	
	@Override
	public Object saveData() {
		return value;
	}
}
