package com.jood;

import org.junit.Assert;
import org.junit.Test;

public class Test1 {

	@Test
	public void testA() {
		Repository repo = new Repository(new TestSettings());
		
		Version version = repo.getRecentVersion();
		
		// todo need to do something better
		MyRoot1 root = version.get(MyRoot1.class);
		
		root.number.set(10);
		MyObjectA a = root.objectA.get();
		a.str.set("yes");
		version.commitSimple(Conflict.USE_MINE);
		
		Version version2 = repo.getVersion(version.getVersionNumber() + 1);
		MyRoot1 root2 = version2.get(MyRoot1.class);
		Assert.assertEquals(Integer.valueOf(10), root2.number.get());
		Assert.assertEquals("yes", root2.objectA.get().str.get());
		root2.number.set(11);
		version2.commitSimple(Conflict.USE_MINE);
		
		Version version3 = repo.getVersion(version2.getVersionNumber() + 1);
		MyRoot1 root3 = version3.get(MyRoot1.class);
		Assert.assertEquals(Integer.valueOf(11), root3.number.get());
		Assert.assertEquals("yes", root3.objectA.get().str.get());
		root3.number.assertUnchanged();
		root3.objectA.get().str.set("no");
		
		
		Version version4 = repo.getVersion(version2.getVersionNumber() + 1);
		version4.get(MyRoot1.class).number.set(12);
		version4.commitSimple(Conflict.USE_MINE);
		
		
		version3.commitSimple(Conflict.USE_THEIRS);

		Version version5 = repo.getVersion(version3.getVersionNumber() + 1);
		MyRoot1 root5 = version5.get(MyRoot1.class);
		Assert.assertEquals(Integer.valueOf(12), root5.number.get());
		Assert.assertEquals("yes", root5.objectA.get().str.get());
	}

	@Test
	public void testB() {
		Repository repo = new Repository(new TestSettings());

		repo.commit(new CommitOperation() {
			@Override
			public void commit(Version version) {

			}
		});

	}
}
