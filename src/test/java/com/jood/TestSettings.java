package com.jood;

import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import com.jood.persist.InMemorySynchronousPersist;
import com.jood.persist.Publisher;
import com.jood.persist.Reporter;
import com.jood.persist.Serializer;
import com.jood.persist.SimpleDefaultJavaSerialization;

public class TestSettings implements DBSettings {

	private InMemorySynchronousPersist persist = new InMemorySynchronousPersist();
	
	@Override
	public Publisher getPublisher() {
		return persist;
	}

	@Override
	public Reporter getReporter() {
		return persist;
	}

	@Override
	public Serializer getSerializer() {
		return new SimpleDefaultJavaSerialization();
	}

	@Override
	public ThreadPoolExecutor getWriteOperationThreadpool() {
		return new ThreadPoolExecutor(1, 1, 0, TimeUnit.SECONDS, new LinkedBlockingQueue<Runnable>());
	}

}
