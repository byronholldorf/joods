package com.jood;

import com.jood.data.DBNumber;
import com.jood.data.DBRef;

public class MyRoot1 {

	public final DBNumber<Integer> number = new DBNumber<Integer>();
	public final DBRef<MyObjectA> objectA = new DBRef<MyObjectA>(MyObjectA.class);

}
